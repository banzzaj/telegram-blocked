package technology.hunters.telegramblocked.tuple;

public class TelegramState {

    public static final TelegramState UNDEFINED = new TelegramState(-1);

    public TelegramState(long nrOfIpsBlocked) {
        this.nrOfIpsBlocked = nrOfIpsBlocked;
    }

    public long nrOfIpsBlocked;
}
