package technology.hunters.telegramblocked.component;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.Message;
import org.telegram.telegrambots.api.objects.Update;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.exceptions.TelegramApiException;
import technology.hunters.telegramblocked.tuple.TelegramState;

@Component
public class Bot extends TelegramLongPollingBot {

    private static final Logger logger = LoggerFactory.getLogger(Bot.class);

    private final Storage storage;

    @Value("${bot.token}")
    private String token;

    @Value("${bot.username}")
    private String username;

    @Autowired
    public Bot(Storage storage) {
        this.storage = storage;
    }

    @Override
    public String getBotToken() {
        return token;
    }

    @Override
    public String getBotUsername() {
        return username;
    }

    @Override
    public void onUpdateReceived(Update update) {

        logger.info("Message received, processing...");

        if (update.hasMessage()) {
            Message message = update.getMessage(); // Received message
            SendMessage response = new SendMessage(); // Answer message
            Long chatId = message.getChatId();
            response.setChatId(chatId);
            String text = message.getText();
            TelegramState state = storage.getState();
            response.setText(String.format("На текущий момент заблокировано адресов: %s", state.nrOfIpsBlocked));
            try {
                execute(response);
                logger.info("Sent message \"{}\" to {}", text, chatId);
            } catch (TelegramApiException e) {
                logger.error("Failed to answer to %s due to error: %s", text, chatId, e.getMessage());
            }
        } else {
            logger.info("Message is empty, skipping...");
        }

        logger.info("Message processed!");
    }

}
