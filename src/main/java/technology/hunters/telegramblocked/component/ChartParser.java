package technology.hunters.telegramblocked.component;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import technology.hunters.telegramblocked.tuple.Point;
import technology.hunters.telegramblocked.tuple.TelegramState;

import java.util.List;

@Component
public class ChartParser {
    private static final Logger LOG = LoggerFactory.getLogger(ChartParser.class);

    public TelegramState parse(List<Point> chart) {
        if (chart == null || chart.isEmpty()) return TelegramState.UNDEFINED;

        Point last = chart.get(chart.size() - 1);

        if (last != null && last.y != null) {
            long ips = last.y;
            LOG.info(String.format("Last ips count: %s", ips));
            return new TelegramState(ips);
        }

        return TelegramState.UNDEFINED;
    }
}
