package technology.hunters.telegramblocked.component;

import org.springframework.stereotype.Component;
import technology.hunters.telegramblocked.tuple.TelegramState;

import java.util.concurrent.atomic.AtomicReference;

@Component
public class Storage {
    private final AtomicReference<TelegramState> state = new AtomicReference<>(TelegramState.UNDEFINED);

    public void setState(TelegramState state) {
        this.state.set(state);
    }

    public TelegramState getState() {
        return this.state.get();
    }
}
