package technology.hunters.telegramblocked;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class TelegramBlockedApplication {

    public static void main(String[] args) {
        SpringApplication.run(TelegramBlockedApplication.class, args);
    }
}
