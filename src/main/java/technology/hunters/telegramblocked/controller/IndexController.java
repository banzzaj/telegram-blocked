package technology.hunters.telegramblocked.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import technology.hunters.telegramblocked.component.Storage;

@Controller
@RequestMapping("/")
public class IndexController {

    private final Storage storage;

    @Autowired
    public IndexController(Storage storage) {
        this.storage = storage;
    }

    @GetMapping("")
    public String index(Model model) {
        model.addAttribute("ips", storage.getState().nrOfIpsBlocked);
        return "index/index";
    }

}
